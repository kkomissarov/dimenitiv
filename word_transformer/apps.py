from django.apps import AppConfig


class WordTransformerConfig(AppConfig):
    name = 'word_transformer'
