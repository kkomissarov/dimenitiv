from django.shortcuts import render, HttpResponse
from django.views import View

def get_dimenitiv(text):
    consonants = ['б', 'в', 'г', 'д', 'ж', 'з', 'к', 'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ь', 'й']
    vowels = ['а', 'е', 'ё', 'и', 'о', 'у', 'ы', 'э', 'ю', 'я']

    text = text.lower()

    #Проверяем что в строке нет запрещенных символов. Если есть, возвращаем тити-мити
    for l in text:
        if l in consonants or l in vowels:
            pass
        else:
            text = 'Тити-мити, жареные гвозди!'
            return text

    #Досим-пердосим
    if text[0:3] == 'дос':
        return text[0].upper()+text[1:]+'-Пер' + text


    #Если титей-митей не оказалось, удаляем согласные буквы из начала слова
    finish = len(text)
    start_string = text
    for l in range(0, finish):
        if start_string[l] in consonants:
            text = text[1:]
        else:
            break

    return start_string[0].upper()+start_string[1:]+'-Шм' + text




class MainPageView(View):
    def get(self, request):
        text = request.GET.get('word', None)

        if text:
            context = {
                'dimenitiv': get_dimenitiv(text),
                'word': text}
        else:
            context = None

        return render(request, 'word_transformer/base.html', context=context)





