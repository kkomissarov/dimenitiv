from django.contrib import admin
from django.urls import path
from word_transformer.views import MainPageView

urlpatterns = [
    path('', MainPageView.as_view(), name="main_page_view"),
]
